package com.pixelscrowd.android;

import android.content.res.Resources;
import android.net.wifi.WifiInfo;
import android.os.Build;
import android.util.DisplayMetrics;

import java.util.HashMap;

public class User {
    String username;
    HashMap<String, String> deviceInfos;

    public User(String email, String MAC) {
        this.username = email.split( "@" )[0];
        this.deviceInfos = new HashMap<>();

        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        deviceInfos.put( "MAC", MAC );
        deviceInfos.put( "name", Build.DEVICE );
        deviceInfos.put( "brand", Build.BRAND );
        deviceInfos.put( "width", String.valueOf( metrics.widthPixels ) );
        deviceInfos.put( "height", String.valueOf( metrics.heightPixels ) );
        deviceInfos.put( "dpi", String.valueOf( metrics.densityDpi ) );
    }
}
