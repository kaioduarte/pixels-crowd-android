package com.pixelscrowd.android;


import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class FirebaseHelper {
    private User myUser;
    private FirebaseUser user;
    private FirebaseAuth auth;
    protected FirebaseDatabase database;

    private Context context;

    public FirebaseHelper() {
        database = FirebaseDatabase.getInstance();
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
    }

    public FirebaseUser getUser() {
        return user;
    }

    public boolean isPhoneSetUp(final User user, String coleta) {
        String path = "/coletas/" + coleta + "/users/" + user.username + "/devices/";
        final Boolean[] flag = {true};

        final DatabaseReference devices = database.getReference( path );
        devices.addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                int index, count;
                count = index = 0;

                for (DataSnapshot o : snapshot.getChildren()) {
                    if (o.child( "MAC" ).getValue().equals( user.deviceInfos.get( "MAC" ) )) {
                        flag[0] = false;
                        myUser.deviceInfos.put("id", String.valueOf( index ));
                    }

                    index = ++count;
                }

                if (flag[0]) {
                    myUser.deviceInfos.put( "id", String.valueOf( index ) );
                    devices.push().setValue( myUser.deviceInfos );
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        } );
        return flag[0];
    }

    public void updateUser(final String MAC) {
        user = auth.getCurrentUser();
    }

    public Task<AuthResult> signIn(String email, String password) {
        return auth.signInWithEmailAndPassword( email, password );
    }

    public void signOut() {
        auth.signOut();
    }

    public String getMacFromFile() {
        try {
            int c;
            String temp = "";
            FileInputStream fin = context.openFileInput( "device" );

            while ((c = fin.read()) != -1) {
                temp += Character.toString( (char) c );
            }

            //string temp contains all the data of the file.
            fin.close();
            return temp;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void insertLocation(HashMap<String, String> values, String coleta) {
        if (user != null) {
            if (myUser == null) {
                myUser = new User( user.getEmail(), getMacFromFile() );
            }

            isPhoneSetUp( myUser, coleta );

            if (myUser.deviceInfos.get( "id" ) == null) return;

            values.put( "device", myUser.deviceInfos.get( "id" ) );
            database.getReference( "/coletas/" + coleta + "/users/" + myUser.username + "/locations/" )
                    .push()
                    .setValue( values );
        }
    }

    public Boolean save(String coleta) {
        Boolean saved;

        try {
            database.getReference( "/coletas/" + coleta ).push().setValue( "sentinel" );
            saved = true;
        } catch (DatabaseException e) {
            e.printStackTrace();
            saved = false;
        }

        return saved;
    }

    public ArrayList<String> retrieve(final Context context, final android.widget.Spinner mColetaSpinner) {
        this.context = context;
        final ArrayList<String> coletas = new ArrayList<>();

        database.getReference( "/coletas/" ).addChildEventListener( new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                fetchData( dataSnapshot, coletas, mColetaSpinner, false );
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                fetchData( dataSnapshot, coletas, mColetaSpinner, true );
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        } );

        return coletas;
    }

    private void fetchData(DataSnapshot snapshot,
                           ArrayList<String> coletas,
                           android.widget.Spinner mColetaSpinner,
                           boolean delete) {
        if (delete) {
            coletas.remove( snapshot.getKey().toString() );
        } else {
            coletas.add( snapshot.getKey().toString() );
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>( this.context, android.R.layout.simple_spinner_item, coletas );
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        mColetaSpinner.setAdapter( adapter );
    }
}
