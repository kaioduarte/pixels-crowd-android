/*
  Copyright 2017 Google Inc. All Rights Reserved.
  <p>
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  <p>
  http://www.apache.org/licenses/LICENSE-2.0
  <p>
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package com.pixelscrowd.android;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.SntpClient;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.ExecutionException;


/**
 * Using location settings.
 * <p/>
 * Uses the {@link com.google.android.gms.location.SettingsApi} to ensure that the device's system
 * settings are properly configured for the app's location needs. When making a request to
 * Location services, the device's system settings may be in a state that prevents the app from
 * obtaining the location data that it needs. For example, GPS or Wi-Fi scanning may be switched
 * off. The {@code SettingsApi} makes it possible to determine if a device's system settings are
 * adequate for the location request, and to optionally invoke a dialog that allows the user to
 * enable the necessary settings.
 * <p/>
 * This sample allows the user to request location updates using the ACCESS_FINE_LOCATION setting
 * (as specified in AndroidManifest.xml).
 */
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private FirebaseHelper firebaseHelper;

    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Code used in requesting runtime permissions.
     */
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    /**
     * Constant used in the location settings dialog.
     */
    private static final int REQUEST_CHECK_SETTINGS = 0x1;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    private final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    private final static String KEY_LOCATION = "location";
    private final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";

    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Provides access to the Location Settings API.
     */
    private SettingsClient mSettingsClient;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private LocationRequest mLocationRequest;

    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    private LocationSettingsRequest mLocationSettingsRequest;

    /**
     * Callback for Location events.
     */
    private LocationCallback mLocationCallback;

    /**
     * Represents a geographical location.
     */
    private Location mCurrentLocation;


    // UI Widgets.
    private Button mStartUpdatesButton;
    private Button mStopUpdatesButton;
    private TextView mLastUpdateTimeTextView;
    private TextView mLatitudeTextView;
    private TextView mLongitudeTextView;
    private TextView mTimeOffsetTextView;
    private Spinner mColetaSpinner;
    private Button mAddColetaButton;

    // Labels.
    private String mLatitudeLabel;
    private String mLongitudeLabel;
    private String mLastUpdateTimeLabel;
    private String mTimeOffsetLabel;

    private Long mTimeOffset;
    private String coletaSelecionada;

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    private Boolean mRequestingLocationUpdates;
    private Boolean flag = false;

    /**
     * Time when the location was updated represented as a Date.
     */
    private Date mLastUpdateTime;

    private void displayInputDialog() {
        final Dialog d = new Dialog( this );
        d.setTitle( "Firebase database" );
        d.setContentView( R.layout.dialog_prompt );

        final EditText nameTxt = d.findViewById( R.id.coletaInput );
        Button saveBtn = d.findViewById( R.id.saveBtn );

        //SAVE
        saveBtn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //GET DATA
                String name = nameTxt.getText().toString();
                Log.println( Log.ERROR, "botao", name );

                //SAVE
                if (name != null && name.length() > 0) {
                    if (firebaseHelper.save( name )) {
                        nameTxt.setText( "" );
                        d.dismiss();
                    }

                } else {
                    Toast.makeText( MainActivity.this, "Name Cannot Be Empty", Toast.LENGTH_SHORT ).show();
                }

            }
        } );

        d.show();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        coletaSelecionada = parent.getSelectedItem().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.main_activity );
        Toolbar toolbar = findViewById( R.id.toolbar );
        setSupportActionBar( toolbar );

        firebaseHelper = new FirebaseHelper();

        // Locate the UI widgets.
        mStartUpdatesButton = findViewById( R.id.start_updates_button );
        mStopUpdatesButton = findViewById( R.id.stop_updates_button );
        mLatitudeTextView = findViewById( R.id.latitude_text );
        mLongitudeTextView = findViewById( R.id.longitude_text );
        mLastUpdateTimeTextView = findViewById( R.id.last_update_time_text );
        mTimeOffsetTextView = findViewById( R.id.time_offset );

        /* NOVO */
        mColetaSpinner = findViewById( R.id.coletaSpinner );
        mColetaSpinner.setOnItemSelectedListener( this );

        firebaseHelper.retrieve( this, mColetaSpinner );

        mAddColetaButton = findViewById( R.id.addColetaBtn );
        mAddColetaButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayInputDialog();
            }
        } );

        // Set labels.
        mLatitudeLabel = getResources().getString( R.string.latitude_label );
        mLongitudeLabel = getResources().getString( R.string.longitude_label );
        mLastUpdateTimeLabel = getResources().getString( R.string.last_update_time_label );
        mTimeOffsetLabel = getResources().getString( R.string.time_offset_label );

        mRequestingLocationUpdates = false;
        mLastUpdateTime = null; // MUDEI

        // Update values using data stored in the Bundle.
        updateValuesFromBundle( savedInstanceState );

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient( this );
        mSettingsClient = LocationServices.getSettingsClient( this );

        // Kick off the process of building the LocationCallback, LocationRequest, and
        // LocationSettingsRequest objects.
        createLocationCallback();
        createLocationRequest();
        buildLocationSettingsRequest();
    }

    @SuppressLint("StaticFieldLeak")
    public long getTimeOffset() {
        final Long[] offset = {0L};
        final SntpClient sntpClient = new SntpClient();
        try {
            new AsyncTask<Void,Integer,Long>(){

                /**
                 * Override this method to perform a computation on a background thread. The
                 * specified parameters are the parameters passed to {@link #execute}
                 * by the caller of this task.
                 * <p/>
                 * This method can call {@link AsyncTask#publishProgress} to publish updates
                 * on the UI thread.
                 *
                 * @param params The parameters of the task.
                 * @return A result, defined by the subclass of this task.
                 * @see AsyncTask#onPreExecute()
                 * @see AsyncTask#onPostExecute
                 * @see AsyncTask#publishProgress
                 */
                @Override
                protected Long doInBackground(Void... params) {
                    Boolean result = sntpClient.requestTime("0.south-america.pool.ntp.org", 30000);

                    if (result) {
                        Date date = new Date(sntpClient.getNtpTime());
                        offset[0] = date.getTime() - new Date().getTime();
                        Log.println( Log.ERROR, "ntp", (date.getTime() - new Date().getTime()) + "" );
                    }

                    return offset[0];
                }
            }.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Toast.makeText( this, "Tempo sincronizado", Toast.LENGTH_SHORT ).show();

        return offset[0];
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.menu_option, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logout:
                Log.println( Log.ERROR, "LOGOUT", "DESLOGANDO" );
                logoutFirebase();
                return true;
            default:
                return super.onOptionsItemSelected( item );
        }
    }

    private void logoutFirebase() {
        firebaseHelper.signOut();
        startActivity( new Intent( MainActivity.this, LoginActivity.class ) );
        finish();
    }

    /**
     * Updates fields based on data stored in the bundle.
     *
     * @param savedInstanceState The activity state saved in the Bundle.
     */
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains( KEY_REQUESTING_LOCATION_UPDATES )) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES );
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains( KEY_LOCATION )) {
                // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable( KEY_LOCATION );
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains( KEY_LAST_UPDATED_TIME_STRING )) {
                mLastUpdateTime = new Date( Long.parseLong( savedInstanceState.getString( KEY_LAST_UPDATED_TIME_STRING ) ) );
            }
            updateUI();
        }
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    @SuppressLint("RestrictedApi")
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval( UPDATE_INTERVAL_IN_MILLISECONDS );

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval( FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS );

        mLocationRequest.setPriority( LocationRequest.PRIORITY_HIGH_ACCURACY );
    }

    /**
     * Creates a callback for receiving location events.
     */
    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult( locationResult );

                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = new Date( mCurrentLocation.getTime() );

                updateLocationFirebase();
                updateLocationUI();
            }
        };
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest( mLocationRequest );
        mLocationSettingsRequest = builder.build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i( TAG, "User agreed to make required location settings changes." );
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i( TAG, "User chose not to make required location settings changes." );
                        mRequestingLocationUpdates = false;
                        updateUI();
                        break;
                }
                break;
        }
    }

    /**
     * Handles the Start Updates button and requests start of location updates. Does nothing if
     * updates have already been requested.
     */
    public void startUpdatesButtonHandler(View view) {
        if (!mRequestingLocationUpdates) {
            setButtonsEnabledState();
            Toast.makeText( this, "Sincronizando tempo", Toast.LENGTH_SHORT ).show();
            mTimeOffset = getTimeOffset();
            mRequestingLocationUpdates = true;
            startLocationUpdates();
        }
    }

    /**
     * Handles the Stop Updates button, and requests removal of location updates.
     */
    public void stopUpdatesButtonHandler(View view) {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        stopLocationUpdates();
    }

    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    private void startLocationUpdates() {
        // Begin by checking if the device has the necessary location settings.
        mSettingsClient.checkLocationSettings( mLocationSettingsRequest )
                .addOnSuccessListener( this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i( TAG, "All location settings are satisfied." );

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates( mLocationRequest,
                                mLocationCallback, Looper.myLooper() );

                        updateUI();
                    }
                } )
                .addOnFailureListener( this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i( TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings " );
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult( MainActivity.this, REQUEST_CHECK_SETTINGS );
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i( TAG, "PendingIntent unable to execute request." );
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e( TAG, errorMessage );
                                Toast.makeText( MainActivity.this, errorMessage, Toast.LENGTH_LONG ).show();
                                mRequestingLocationUpdates = false;
                        }

                        updateUI();
                    }
                } );
    }

    /**
     * Updates all UI fields.
     */
    private void updateUI() {
        setButtonsEnabledState();
        updateLocationUI();
    }

    private void updateLocationFirebase() {
        if (mCurrentLocation != null) {
            HashMap<String, String> coordinates = new HashMap<>();
            coordinates.put( "latitude", String.valueOf( mCurrentLocation.getLatitude() ) );
            coordinates.put( "longitude", String.valueOf( mCurrentLocation.getLongitude() ) );
            coordinates.put( "accuracy", String.valueOf( mCurrentLocation.getAccuracy() ) );
            coordinates.put( "timestamp", String.valueOf( mLastUpdateTime.getTime() + mTimeOffset ) );
            firebaseHelper.insertLocation( coordinates, coletaSelecionada );
            Log.println( Log.ERROR, "TIME ANTES", "" + mLastUpdateTime.getTime());
            Log.println( Log.ERROR, "TIME DEPOIS", "" + (mLastUpdateTime.getTime() + mTimeOffset) );
        }
    }

    /**
     * FIX
     * <p>
     * Disables both buttons when functionality is disabled due to insuffucient location settings.
     * Otherwise ensures that only one button is enabled at any time. The Start Updates button is
     * enabled if the user is not requesting location updates. The Stop Updates button is enabled
     * if the user is requesting location updates.
     */
    private void setButtonsEnabledState() {
        if (mRequestingLocationUpdates) {
            mStartUpdatesButton.setEnabled( false );
            mStopUpdatesButton.setEnabled( true );
        } else {
            mStartUpdatesButton.setEnabled( true );
            mStopUpdatesButton.setEnabled( false );
        }
    }

    /**
     * Sets the value of the UI fields for the location latitude, longitude and last update time.
     */
    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            mLatitudeTextView.setText( String.format( Locale.ENGLISH, "%s: %f", mLatitudeLabel,
                    mCurrentLocation.getLatitude() ) );
            mLongitudeTextView.setText( String.format( Locale.ENGLISH, "%s: %f", mLongitudeLabel,
                    mCurrentLocation.getLongitude() ) );
            mLastUpdateTimeTextView.setText( String.format( Locale.ENGLISH, "%s: %s",
                    mLastUpdateTimeLabel, DateFormat.getTimeInstance().format( mLastUpdateTime ) ) );
            mTimeOffsetTextView.setText( String.format( Locale.ENGLISH, "%s: %s",
                    mTimeOffsetLabel, DateFormat.getTimeInstance().format(
                            new Date(mLastUpdateTime.getTime() + mTimeOffset) ) ) );
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    private void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) {
            Log.d( TAG, "stopLocationUpdates: updates never requested, no-op." );
            return;
        }

        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        mFusedLocationClient.removeLocationUpdates( mLocationCallback )
                .addOnCompleteListener( this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mRequestingLocationUpdates = false;
                        mCurrentLocation = null;
                        setButtonsEnabledState();
                    }
                } );
    }

    @Override
    public void onResume() {
        super.onResume();
        if (flag) {
            mRequestingLocationUpdates = true;
        }

        // Within {@code onPause()}, we remove location updates. Here, we resume receiving
        // location updates if the user has requested them.
        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        } else if (!checkPermissions()) {
            requestPermissions();
        }

        updateUI();
    }

    @Override
    protected void onPause() {
        super.onPause();

        flag = mRequestingLocationUpdates && true;

        // Remove location updates to save battery.
        stopLocationUpdates();
    }

    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean( KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates );
        savedInstanceState.putParcelable( KEY_LOCATION, mCurrentLocation );
        savedInstanceState.putString( KEY_LAST_UPDATED_TIME_STRING, String.valueOf( mLastUpdateTime ) );
        super.onSaveInstanceState( savedInstanceState );
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById( android.R.id.content ),
                getString( mainTextStringId ),
                Snackbar.LENGTH_INDEFINITE )
                .setAction( getString( actionStringId ), listener ).show();
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission( this,
                Manifest.permission.ACCESS_FINE_LOCATION );
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale( this,
                        Manifest.permission.ACCESS_FINE_LOCATION );

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i( TAG, "Displaying permission rationale to provide additional context." );
            showSnackbar( R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions( MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE );
                        }
                    } );
        } else {
            Log.i( TAG, "Requesting permission" );
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions( MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE );
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i( TAG, "onRequestPermissionResult" );
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i( TAG, "User interaction was cancelled." );
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (mRequestingLocationUpdates) {
                    Log.i( TAG, "Permission granted, updates requested, starting location updates" );
                    startLocationUpdates();
                }
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar( R.string.permission_denied_explanation,
                        R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS );
                                Uri uri = Uri.fromParts( "package",
                                        BuildConfig.APPLICATION_ID, null );
                                intent.setData( uri );
                                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                                startActivity( intent );
                            }
                        } );
            }
        }
    }

}
