package com.pixelscrowd.android;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


// FIREBASE
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import java.io.FileOutputStream;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;


public class LoginActivity extends AppCompatActivity {

    private Button btnLogin;
    private EditText inputEmail, inputPassword;
    private FirebaseHelper firebaseHelper;
    private static boolean flag = true;

    void redirectLoggedIn() {
        startActivity( new Intent( LoginActivity.this, MainActivity.class ) );
        finish();
    }

    public String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list( NetworkInterface.getNetworkInterfaces() );
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase( "wlan0" )) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append( Integer.toHexString( b & 0xFF ) + ":" );
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt( res1.length() - 1 );
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return "";
    }

    void mapUIElements() {
        Toolbar toolbar = findViewById( R.id.toolbar );
        setSupportActionBar( toolbar );

        inputEmail = findViewById( R.id.email );
        inputPassword = findViewById( R.id.password );
        btnLogin = findViewById( R.id.btn_login );

        btnLogin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = inputEmail.getText().toString();
                String password = inputPassword.getText().toString();

                if (!Utils.validateEmail( getApplicationContext(), email )
                        || !Utils.validatePassword( getApplicationContext(), password )) {
                    return;
                }

                firebaseHelper.signIn( email, password )
                        .addOnCompleteListener( LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if (!task.isSuccessful()) {
                                    Utils.showToast( getApplicationContext(), "Falha ao logar" );
                                } else {
                                    firebaseHelper.updateUser( getMacAddr() );
                                    redirectLoggedIn();
                                }
                            }
                        } );
            }
        } );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        if (LoginActivity.flag) {
            com.google.firebase.database.FirebaseDatabase.getInstance().setPersistenceEnabled( true );
            LoginActivity.flag = false;
        }

        firebaseHelper = new FirebaseHelper();

        if (firebaseHelper.getUser() != null) {
            String filename = "device";
            FileOutputStream outputStream;

            try {
                outputStream = openFileOutput(filename, this.MODE_PRIVATE);
                outputStream.write(getMacAddr().getBytes());
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            redirectLoggedIn();
        }

        setContentView( R.layout.activity_login );
        mapUIElements();
    }
}
